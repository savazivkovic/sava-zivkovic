﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace Proizvodi
{
    public class Klase_Podataka
    {
        private string StringKonekcije;


        public Klase_Podataka(string NoviStringKonekcije)
        {
            StringKonekcije = NoviStringKonekcije;
        }

        public void proizvodi(int id, string naziv, string opis, string kategorija, string proizvodjac, string dobavljac, float cena)
        {

            string ImeProcedure = "SelektujParametre";
            SqlConnection konekcija = new SqlConnection(StringKonekcije);
            SqlCommand Komanda = new SqlCommand(ImeProcedure, konekcija);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.AddWithValue("@ID", id);
            Komanda.Parameters.AddWithValue("@Naziv", naziv);
            Komanda.Parameters.AddWithValue("@Opis", opis);
            Komanda.Parameters.AddWithValue("@Kategorija", kategorija);
            Komanda.Parameters.AddWithValue("@Proizvodjac", proizvodjac);
            Komanda.Parameters.AddWithValue("@Dobavljac", dobavljac);
            Komanda.Parameters.AddWithValue("@Cena", cena);

            konekcija.Open();
            Komanda.ExecuteNonQuery();
            konekcija.Close();

        }

        public DataSet UzmiID(int Id)
        {
            

                string ImePricedure = "SelektujID";
                DataSet ds = new DataSet();
                SqlConnection konekcija = new SqlConnection(StringKonekcije);
                konekcija.Open();
                SqlCommand Komanda = new SqlCommand(ImePricedure, konekcija);
                Komanda.CommandType = CommandType.StoredProcedure;
                Komanda.Parameters.Add("@ID", SqlDbType.Int).Value = Id;
                SqlDataAdapter da = new SqlDataAdapter(Komanda);
               
                da.Fill(ds);
                konekcija.Close();

                return ds;
            }
            
          
    }
}