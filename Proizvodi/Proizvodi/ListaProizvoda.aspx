﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ListaProizvoda.aspx.cs" Inherits="Proizvodi.ListaProizvoda" %>




<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <div style="height: 443px; margin-left: 20px; margin-top: 118px">
    <asp:GridView ID="GridView_Lista" runat="server" CellPadding="4" ForeColor="#333333" GridLines="Vertical" Height="187px" Width="485px" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="Lista_SelectedIndexChanged" style="margin-left: 0px; margin-top: 106px" AllowPaging="True" AllowSorting="True">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
            <asp:BoundField DataField="Naziv" HeaderText="Naziv" SortExpression="Naziv" />
            <asp:BoundField DataField="Opis" HeaderText="Opis" SortExpression="Opis" />
            <asp:BoundField DataField="Kategorija" HeaderText="Kategorija" SortExpression="Kategorija" />
            <asp:BoundField DataField="Proizvodjac" HeaderText="Proizvodjac" SortExpression="Proizvodjac" />
            <asp:BoundField DataField="Dobavljac" HeaderText="Dobavljac" SortExpression="Dobavljac" />
            <asp:BoundField DataField="Cena" HeaderText="Cena" SortExpression="Cena" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:KONEKCIJA_DB %>" SelectCommand="SELECT * FROM [proizvodi]" DeleteCommand="DELETE FROM [proizvodi] WHERE [ID] = @ID" InsertCommand="INSERT INTO [proizvodi] ([ID], [Naziv], [Opis], [Kategorija], [Proizvodjac], [Dobavljac], [Cena]) VALUES (@ID, @Naziv, @Opis, @Kategorija, @Proizvodjac, @Dobavljac, @Cena)" UpdateCommand="UPDATE [proizvodi] SET [Naziv] = @Naziv, [Opis] = @Opis, [Kategorija] = @Kategorija, [Proizvodjac] = @Proizvodjac, [Dobavljac] = @Dobavljac, [Cena] = @Cena WHERE [ID] = @ID">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="ID" Type="Int32" />
                <asp:Parameter Name="Naziv" Type="String" />
                <asp:Parameter Name="Opis" Type="String" />
                <asp:Parameter Name="Kategorija" Type="String" />
                <asp:Parameter Name="Proizvodjac" Type="String" />
                <asp:Parameter Name="Dobavljac" Type="String" />
                <asp:Parameter Name="Cena" Type="Double" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Naziv" Type="String" />
                <asp:Parameter Name="Opis" Type="String" />
                <asp:Parameter Name="Kategorija" Type="String" />
                <asp:Parameter Name="Proizvodjac" Type="String" />
                <asp:Parameter Name="Dobavljac" Type="String" />
                <asp:Parameter Name="Cena" Type="Double" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
</div>


</asp:Content>
