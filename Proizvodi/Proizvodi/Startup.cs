﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Proizvodi.Startup))]
namespace Proizvodi
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
