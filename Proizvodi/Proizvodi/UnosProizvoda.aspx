﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="UnosProizvoda.aspx.cs" Inherits="Proizvodi.UnosProizvoda" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
  
    <div style="height: 659px; margin-left: 441px; margin-top: 167px">
    ID<br />
    <asp:TextBox ID="ID" runat="server" Width="68px" Height="22px" OnTextChanged="ID_TextChanged"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="ID" ErrorMessage="Mozete uneti samo broj!" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
    <br />
    Naziv<br />
    <asp:TextBox ID="Naziv" runat="server"></asp:TextBox>
    <br />
    Opis<br />
        <asp:TextBox ID="Opis" runat="server" Height="69px" TextMode="MultiLine"></asp:TextBox>
        <br />
    Kategorija<br />
    <asp:TextBox ID="Kategorija" runat="server" OnTextChanged="Kategorija_TextChanged"></asp:TextBox>
    <br />
    Proizvodjac<br />
    <asp:TextBox ID="Proizvodjac" runat="server"></asp:TextBox>
    <br />
    Dobavljac<br />
    <asp:TextBox ID="Dobavljac" runat="server"></asp:TextBox>
    <br />
    Cena<br />
    <asp:TextBox ID="Cena" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Cena" ErrorMessage="Mozete uneti samo broj!" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
        <br />
        <br />
&nbsp;<asp:Button ID="Snimi" runat="server" BackColor="#336699" ForeColor="White" Text="SNIMI" Width="128px" OnClick="Snimi_Click" />
    <br />
        <br />
        <asp:Label ID="LabelaUpozorenje" runat="server" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="LabelaID" runat="server" Visible="False"></asp:Label>
</div>
  
</asp:Content>