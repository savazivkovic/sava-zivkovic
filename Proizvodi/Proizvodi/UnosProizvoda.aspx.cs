﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Newtonsoft.Json;
using System.IO;

using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;


namespace Proizvodi
{
    public partial class UnosProizvoda : System.Web.UI.Page
    {
        private Klase_Podataka Proizvodi;
        
        
        

        protected void Page_Load(object sender, EventArgs e)
        {
            Proizvodi = new Klase_Podataka(ConfigurationManager.ConnectionStrings["KONEKCIJA_DB"].ConnectionString);
        }

        protected void ID_TextChanged(object sender, EventArgs e)
        {

        }

       

        protected void Snimi_Click(object sender, EventArgs e)
        {


            if (ID.Text == "" || Naziv.Text == "" || Opis.Text == "" || Kategorija.Text == "" || Proizvodjac.Text == "" || Dobavljac.Text == "" || Cena.Text == "")
            {


                LabelaUpozorenje.Visible = true;
                LabelaUpozorenje.Text = "Svi podaci moraju biti uneti!";
                LabelaUpozorenje.ForeColor = System.Drawing.Color.Red;
            }


           
            else
            {
                
                    int Id = int.Parse(ID.Text);
                    DataSet ds = new DataSet();
                    ds = Proizvodi.UzmiID(Id);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        LabelaUpozorenje.Visible = true;
                        LabelaUpozorenje.Text = "ID sa ovim brojem vec postoji!";
                        LabelaUpozorenje.ForeColor = System.Drawing.Color.Red;

                    }
                
                else
                {


                   

                    string naziv = Naziv.Text;
                    string opis = Opis.Text;
                    string kategorija = Kategorija.Text;
                    string proizvodjac = Proizvodjac.Text;
                    string dobavljac = Dobavljac.Text;
                    float cena = float.Parse(Cena.Text);

                    Proizvodi = new Klase_Podataka(ConfigurationManager.ConnectionStrings["KONEKCIJA_DB"].ConnectionString);
                    Proizvodi.proizvodi(Id, naziv, opis, kategorija, proizvodjac, dobavljac, cena);

                    LabelaUpozorenje.Text = "Podaci su uspesno snimljeni";
                    LabelaUpozorenje.ForeColor = System.Drawing.Color.Blue;
                    LabelaUpozorenje.Visible = true;

                   

                }

                

            }
            //string JsonString = JsonConvert.SerializeObject(Proizvodi);

            //File.WriteAllText(@"proizvodi.json", JsonString);
           

        }




        protected void Kategorija_TextChanged(object sender, EventArgs e)
        {

        }
    }
}